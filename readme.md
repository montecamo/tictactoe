# Splashy

[![react](https://aleen42.github.io/badges/src/react.svg)](https://aleen42.github.io/badges/src/react.svg)
[![redux](https://aleen42.github.io/badges/src/redux.svg)](https://aleen42.github.io/badges/src/redux.svg)
[![webpack](https://aleen42.github.io/badges/src/webpack.svg)](https://aleen42.github.io/badges/src/webpack.svg)

Your next wallpaper.

## Installation

### Linux

1. Clone the source locally:

```sh
$ git clone https://github.com/montecamo/splashy
$ cd frontend
```

2. Install project dependencies:

```sh
$ npm install
```

3. Start the application:

```sh
$ npm run serve
```
