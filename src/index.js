import React from 'react';
import { render } from 'react-dom';

import 'Styles/index.scss';

import Main from 'Views/Main';

const App = () => <Main />;

export default App;

render(<App />, document.getElementById('root'));
