import React from 'react';

import Home from 'Views/Home';

const IndexRouter = () => {
  return <Home />;
};

export default IndexRouter;
