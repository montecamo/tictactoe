const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const srcDir = path.resolve(__dirname, '../src');

let config = {
  context: srcDir,

  entry: {
    app: './index.js',
  },

  resolve: {
    alias: {
      Components: path.resolve(srcDir, 'components/'),
      Store: path.resolve(srcDir, 'store/'),
      Modules: path.resolve(srcDir, 'modules/'),
      Styles: path.resolve(srcDir, 'styles/'),
      Views: path.resolve(srcDir, 'views/'),
      Api: path.resolve(srcDir, 'api/'),
    },
    extensions: ['.js', '.jsx', '.scss'],
  },

  output: {
    filename: 'main.js',
    path: path.join(__dirname, '../dist'),
  },

  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, '../public/index.html'),
    }),

    new ExtractTextPlugin('style.css'),
  ],

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: ['css-loader', 'sass-loader'],
        }),
      },
      {
        test: /\.(woff(2)?|ttf|eot|jpg|svg|ico)(\?v=\d+\.\d+\.\d+)?$/,
        use: ['file-loader?name=[name].[ext]'],
      },
    ],
  },
};

module.exports = config;
